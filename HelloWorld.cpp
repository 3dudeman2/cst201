#include <iostream>

using namespace std; //JLK: This is helpful, but there are reasons that we want "using std::cout;" instead.

int main(){
	cout << "hello world!" << endl; //JLK: This works
	return 0;
}